### change admin password
.ONESHELL:
.PHONY: wso2-change-password
wso2-change-password:
        @echo "old_password"
        @read old_password
        @echo "new_password"
        @read new_password
        @eval java -jar chdo-password-changer-1.0.0-SNAPSHOT-jar-with-dependencies.jar $$old_password $$new_password
        if [ "$$?" = 0 ]; then \
                docker-compose down;\
                docker-compose up -d;\
        fi
        exit 0

### Infrastructure

.PHONY: up 
up:
	docker-compose up -d

.PHONY: down 
down:
	docker-compose down

.PHONY:  ps 
ps:
	docker-compose ps 

.PHONY:  logs 
logs:
	docker-compose logs -f
