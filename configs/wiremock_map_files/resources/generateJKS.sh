#!/bin/bash

# Generate java key store for wiremock
keytool -genkey -alias wiremock -dname "CN=wiremock1.int.org-cs-0.org.innovateuk.ukri.org, OU=Government Digital Service, O=Government Digital Service, L=London, S=Greater London, C=GB" -keyalg RSA -keysize 1024 -validity 365 -keypass wso2carbon -keystore wso2carbon.jks -storepass wso2carbon 

# Create the self-signed certificate
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout wiremock.key -out wiremock.crt -subj "/C=GB/ST=Greater London/L=London/O=Government Digital Service/OU=Government Digital Service/CN=wiremock1.int.org-cs-0.org.innovateuk.ukri.org"

#Import certification into keystore
keytool -noprompt -import -trustcacerts -alias wire -file wiremock.crt -keystore wso2carbon.jks -storepass wso2carbon