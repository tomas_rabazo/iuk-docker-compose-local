openssl genrsa -des3 -passout pass:chakray -out temp.pem 2048

openssl req -new -key temp.pem -passin pass:chakray -subj "/CN=*.sandbox.dev.chakray.cloud" -out temp.csr

openssl rsa -in temp.pem -passin pass:chakray -out auto-signed-certificate.key

openssl x509 -req -days 365 -in temp.csr -signkey auto-signed-certificate.key -out auto-signed-certificate.crt

rm temp.pem

rm temp.csr
