# [INVE] docker-compose-local

https://bitbucket.org/tomas_rabazo/iuk-docker-compose-local/src/develop/

> SOLO PARA DESARROLLO EN LOCAL. NO INCLUYE APIM

![picture](images/docker-compose.png)

# Instalación del producto WSO2 EI CON MySQL #

## 1. Versiones activas

| Producto      | Version | Comentarios |
| ---           | ---     | ---         |
| WIREMOCK      | 2.21.0  |             |
| MYSQL         | 5.7.26  |             |
| NGINX         | 1.17.1  |             |
| WSO2 MB       | 6.6.0   |             |
| WSO2 EI       | 6.4.0   |             |

## 2. URL de acceso a los productos

WSO2 EI

- http://ei-local.iuk.com
- https://ei-local.iuk.com
- https://mgt-ei-local.iuk.com (/carbon)

WSO2 MB

- https://mgt-ei-local.iuk.com:9446/carbon/admin/login.jsp

Por defecto la contraseña es admin/admin

## 3. Instalación

### Requerimientos el sistema

__Software__

- docker
- docker-compose

### Instalación y arranque

1. Se clona el repositorio: git clone <REPO_URL>
2. Se arranca el docker-compose con el comando "docker-compose up --force-recreate -d"
3. Wiremock utiliza un certificado.

   Importar certificado del Wiremock: 
   
   /docker-compose-local/configs/wiremock_map_files/resources/wiremock1.int.org-cs-0.org.innovateuk.ukri.org.cer

   Al EI:

   /docker-compose-local/configs/wso2-ei/repository/resources/security/client-truststore.jks
   >keytool -import -keystore client-truststore.jks -file wiremock1.int.org-cs-0.org.innovateuk.ukri.org.cer -alias wiremock-server

#### Acceso sin resolución DNS

Al ser un entorno local, no tendremos resolución por DNS configurados en internet, no podrás acceder a los productos si no defines estos nombres en tu archivo /etc/hosts apuntando a tu máquina arrancando el docker-compose.

```
127.0.0.1       ei-local.iuk.com mgt-ei-local.iuk.com
```

### Memoria RAM

| Producto      | Memoría |
| ---           | ---     |
| mysql         | 250M    |
| nginx         | 250M    |
| ei            | 1G      |
| ---           | ---     |
| Total         | 1.5G    |

### Persistencia

No se persiste ningún volumen en la máquina anfitriona

### Gestión de los certificados

Los certificados frontales expuestos a Internet se deben configurar en "volumes/nginx/ssl" y deben añadirse en los keystores de tipo "client-truststore.jks".

Los certificados internos que por nombre deberán de ser configurados se encuentran en:

- volumes/wso2-ei/repository/resources/security

- keytool -import -keystore configs/wso2-ei/repository/resources/security/client-truststore.jks -file configs/nginx/ssl/auto-signed-certificate.crt -alias ssl-server

Por facilitad, estos deberán de ser los mismos entre ellos para permitir la comunicación.